"""分子関連"""
from collections.abc import Iterator, Sequence
from typing import IO
import os
from .typehint import Sphere, Vector3f


class Mol:
    """PDBから使う情報のみを取得可能にしたラッパー"""

    def __init__(self,
                 atom_spheres: Sequence[Sphere],
                 bonds: Sequence[tuple[int, int]]):
        self._atom_s = atom_spheres
        self._bonds = bonds

    def atom_idxs(self) -> Iterator[int]:
        return range(len(self._atom_s))

    def get_vdw_sphere_from_atom_idx(
            self, atom_idx: int, ) -> Sphere:
        """原子インデックスに対応するファンデルワールス半径の球でを返す.

        Args:
            atom_idx: 原子インデックス
        Returns:
            ファンデルワールス半径の球
        """
        return self._atom_s[atom_idx]

    def get_bonds(self) -> Iterator[tuple[int, int]]:
        return iter(self._bonds)


def _read_pdb_atom_line(line: str) -> tuple[int, Vector3f, float]:
    atom_id = int(line[6:11])
    try:
        x = float(line[30:38])
        y = float(line[38:46])
        z = float(line[46:54])
        symbol = line[76:78].strip()
        if symbol != 'H':
            radius = symbol_to_radius[symbol]
            return (atom_id, (x, y, z), radius)
    except ValueError | IndexError:
        pass
    return (atom_id, None, None)


def _read_pdb_connect_line(line: str) -> tuple[int, Sequence[int]]:
    """

    Returns:
        (元原子のID, 接続先原子のID集合)
    """
    NUM_LEN = 5
    source = int(line[6:11])
    neg = []
    for i in range(4):
        start = 11 + i * NUM_LEN
        end = start + NUM_LEN
        if len(line) < end:
            break
        val_str = line[start:end].strip()
        if val_str.isdigit():
            neg.append(int(line[start:end]))
    return (source, neg)


def create_mol_from_file(path: str | bytes | os.PathLike) -> Mol:
    with open(path, 'r') as f:
        return create_mol_from_stream(f)


def create_mol_from_stream(fin: IO[str]) -> Mol:
    max_atom_id = -1
    atom_id_to_info: dict[int, Sphere] = dict()
    bond_id_list: list[int, int] = []
    for line in fin:
        if line.startswith('ATOM'):
            atom_id, pos, atom_r = _read_pdb_atom_line(line)
            if max_atom_id < atom_id:
                max_atom_id = atom_id
            if atom_r is not None:
                atom_id_to_info[atom_id] = (pos, atom_r)
        elif line.startswith('CONECT'):
            src_id, neg_ids = _read_pdb_connect_line(line)
            for neg_id in neg_ids:
                if src_id < neg_id:
                    bond_id_list.append((src_id, neg_id))
    atom_id_to_idx: dict[int, int] = dict()
    atom_list: list[float] = []
    for i in range(max_atom_id + 1):
        cur_s = atom_id_to_info.get(i, None)
        if cur_s is not None:
            atom_id_to_idx[i] = len(atom_list)
            atom_list.append(cur_s)
    bond_idx_list: list[tuple[int, int]] = []
    for b in bond_id_list:
        start = atom_id_to_idx.get(b[0], None)
        end = atom_id_to_idx.get(b[1], None)
        if (start is not None) and (end is not None):
            bond_idx_list.append((start, end))
    return Mol(atom_list, bond_idx_list)


symbol_to_radius = {
        'H': 1.2,
        'He': 1.4,
        'Li': 2.2,
        'Be': 1.9,
        'B': 1.8,
        'C': 1.7,
        'N': 1.6,
        'O': 1.55,
        'F': 1.5,
        'Ne': 1.54,
        'Na': 2.4,
        'Mg': 2.2,
        'Al': 2.1,
        'Si': 2.1,
        'P': 1.95,
        'S': 1.8,
        'Cl': 1.8,
        'Ar': 1.88,
        'K': 2.8,
        'Ca': 2.4,
        'Sc': 2.3,
        'Ti': 2.15,
        'V': 2.05,
        'Cr': 2.05,
        'Mn': 2.05,
        'Fe': 2.05,
        'Co': 2.0,
        'Ni': 2.0,
        'Cu': 2.0,
        'Zn': 2.1,
        'Ga': 2.1,
        'Ge': 2.1,
        'As': 2.05,
        'Se': 1.9,
        'Br': 1.9,
        'Kr': 2.02,
        'Rb': 2.9,
        'Sr': 2.55,
        'Y': 2.4,
        'Zr': 2.3,
        'Nb': 2.15,
        'Mo': 2.1,
        'Tc': 2.05,
        'Ru': 2.05,
        'Rh': 2.0,
        'Pd': 2.05,
        'Ag': 2.1,
        'Cd': 2.2,
        'In': 2.2,
        'Sn': 2.25,
        'Sb': 2.2,
        'Te': 2.1,
        'I': 2.1,
        'Xe': 2.16,
        'Cs': 3.0,
        'Ba': 2.7,
        'La': 2.5,
        'Ce': 2.48,
        'Pr': 2.47,
        'Nd': 2.45,
        'Pm': 2.43,
        'Sm': 2.42,
        'Eu': 2.4,
        'Gd': 2.38,
        'Tb': 2.37,
        'Dy': 2.35,
        'Ho': 2.33,
        'Er': 2.32,
        'Tm': 2.3,
        'Yb': 2.28,
        'Lu': 2.27,
        'Hf': 2.25,
        'Ta': 2.2,
        'W': 2.1,
        'Re': 2.05,
        'Os': 2.0,
        'Ir': 2.0,
        'Pt': 2.05,
        'Au': 2.1,
        'Hg': 2.05,
        'Tl': 2.2,
        'Pb': 2.3,
        'Bi': 2.3,
        'Po': 2.0,
        'At': 2.0,
        'Rn': 2.0,
        'Fr': 2.0,
        'Ra': 2.0,
        'Ac': 2.0,
        'Th': 2.4,
        'Pa': 2.0,
        'U': 2.3,
        'Np': 2.0,
        'Pu': 2.0,
        'Am': 2.0,
        'Cm': 2.0,
        'Bk': 2.0,
        'Cf': 2.0,
        'Es': 2.0,
        'Fm': 2.0,
        'Md': 2.0,
        'No': 2.0,
        'Lr': 2.0,
        'Rf': 2.0,
        'Db': 2.0,
        'Sg': 2.0,
        'Bh': 2.0,
        'Hs': 2.0,
        'Mt': 2.0,
        'Ds': 2.0,
        'Rg': 2.0,
        'Cn': 2.0,
        'Nh': 2.0,
        'Fl': 2.0,
        'Mc': 2.0,
        'Lv': 2.0,
        'Ts': 2.0,
        'Og': 2.0,
}
