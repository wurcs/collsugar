"""機械学習結果からの分類のメーインルーチン"""
import argparse
from collections.abc import Iterator
import pathlib
import pickle
from typing import IO
from sklearn import svm
from . import glycam
from . import iupac
from . import rotama


def create_parser():
    """コマンドラインオプション設定"""
    parser = argparse.ArgumentParser(
            description='iupac糖鎖文字列から衝突判定を機械学習を用いて行う')
    parser.add_argument(
            'out_path', help='出力ファイルのパス', type=pathlib.Path)
    parser.add_argument(
            'model', help='学習済みモデルの保存ディレクトリ',
            type=pathlib.Path)
    parser.add_argument(
            '--input', help='入力ファイルのパス,デフォルトは標準入力',
            type=pathlib.Path)
    parser.add_argument(
            '--tmpdir', help='作業用tmpディレクトリのパス',
            type=pathlib.Path, default=pathlib.Path('/tmp/collearn'))
    return parser


class ModelManager:
    """学習モデルディレクトリの管理"""

    def __init__(self, model_dir: pathlib.Path):
        self._len_to_model: dict[int, svm.SVC] = dict()
        self._reverse_len: list[int] = list()
        for f in model_dir.iterdir():
            if str.isdigit(f.stem) and (f.suffix == '.pkl'):
                n = int(f.stem)
                self._reverse_len.append(n)
        self._reverse_len.sort(reverse=True)
        self._dir_path = model_dir

    def get_model(self, length: int) -> svm.SVC:
        """指定された配列長の学習モデルを返す"""
        if length in self._len_to_model:
            model = self._len_to_model[length]
        else:
            model_path = self._dir_path / (str(length) + '.pkl')
            with open(model_path, 'rb') as f:
                model = pickle.load(f)
            self._len_to_model[length] = model
        return model

    def get_len(self, max_len: int | None = None) -> Iterator[int]:
        """学習モデルの配列長を長い順に返す

        Args:
            max_len: Iteratorに含む最大の配列長
        """
        for i in self._reverse_len:
            if i <= max_len:
                yield i


def collides_iupac(iupac_str: str, model_manager: ModelManager,
                   gen_rotamer_path: pathlib.Path,
                   ) -> Iterator[tuple[str, bool]]:
    iupac_len = iupac_str.count('-') - 1
    try:
        model_len = next(model_manager.get_len(iupac_len))
    except StopIteration:
        return
    model = model_manager.get_model(model_len)
    for rotate in glycam.gen_rotate_info(iupac_str, gen_rotamer_path):
        sugar_chain = iupac.str_sequence_to_int_array(iupac_str)
        rotate_info = rotama.rotate_str_to_int(rotate, len(sugar_chain))
        collides = False
        for i in range(iupac_len - model_len + 1):
            ret = model.predict(((*sugar_chain[i:i+model_len],
                                  *rotate_info[i:i+model_len]), ))
            if ret[0]:
                collides = True
                break
        yield (rotate, collides)


def routine(out: IO[str], iupac_src: IO[str], model_dir: pathlib.Path,
            gen_rotamer_path: pathlib.Path,
            ) -> None:
    model_manager = ModelManager(model_dir)

    for line in iupac_src:
        iupac_str = line.strip()
        if len(iupac_str) == 0:
            continue
        for rotate, col in collides_iupac(
                iupac_str, model_manager, gen_rotamer_path):
            out.write(iupac_str)
            out.write(',')
            out.write(rotate)
            out.write(',')
            out.write('col' if col else 'safe')
            out.write('\n')


def main():
    args = create_parser().parse_args()
    gen_rotamer_path = (pathlib.Path(__file__).parent.parent
                        / 'glycam_rotamer/genrotamer')
    with open(args.out_path, 'w') as out, \
         open(args.input, 'r') as iupac:
        routine(out, iupac, args.model, gen_rotamer_path)
