from typing import TypeAlias
import numpy as np

Vector3f: TypeAlias = tuple[float, float, float] | np.ndarray
Sphere: TypeAlias = tuple[Vector3f, float]
