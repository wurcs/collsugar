import argparse
import pathlib
from . import chem
from . import collision


def create_parser():
    """コマンドラインオプション設定"""
    parser = argparse.ArgumentParser(description='PDB分子の衝突判定を行う')
    parser.add_argument('src_mol', help='分子データのファイルのパス',
                        type=pathlib.Path)
    return parser


def main():
    args = create_parser().parse_args()
    mol = chem.mol_from_file(args.src_mol)
    spheres = ((i, mol.get_vdw_sphere_from_atom_idx(i))
               for i in mol.atom_idxs())
    spheres = map(lambda s: (s[0], (s[1][0], 0.45 * s[1][1])), spheres)
    broad_col = collision.sweep_and_prune(spheres)
    col = collision.strict_collision(
            iter(broad_col), mol.get_vdw_sphere_from_atom_idx)
    bonds = set(mol.get_bonds())
    col = filter((lambda c: not (c in bonds)), col)
    try:
        next(col)
        print('col')
    except StopIteration:
        print('safe')
