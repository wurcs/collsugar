from collections.abc import Iterator, Sequence
import random
import itertools
import re


# non rotate
# DGalpb1-4DGlcpNAcb1-OH
# DGalpa1-3DGalpNAca1-OH
# rotate
# DGalpNAca1-6DGalpNAca1-OH
# DNeup5Aca2-3DGalpb1-OH
# DNeup5Aca2-6DGalpb1-OH

_single_sequence = (
    'DGalpb1-4',
    'DGalpa1-3',
    'DGalpNAca1-6',
    'DNeup5Aca2-3',
    'DNeup5Aca2-6',
)

_sequence_to_int = {
    (1, 4): 0,
    (1, 3): 1,
    (1, 6): 2,
    (2, 3): 3,
    (2, 6): 4,
    (1, 2): 5,
}

end_single_sequence = 'DGalpb1-OH'


def generate_sequence(itr: Iterator[int]) -> str:
    """整数配列からiupack形式の糖鎖文字列を生成する."""
    return ''.join(itertools.chain(
        single_sequence_iterator(itr),
        iter((end_single_sequence, )),
    ))


def single_sequence_iterator(itr: Iterator[int]) -> Iterator[str]:
    return map(lambda i: _single_sequence[i], itr)


def str_sequence_to_int_array(sequence: str) -> Sequence[int]:
    """iupack形式の糖鎖文字列を対応する整数配列に変換する."""
    int_list = []
    for m in re.finditer(r'-', sequence):
        div_i = m.start()
        p = sequence[div_i - 1]
        n = sequence[div_i + 1]
        if p.isdigit() and n.isdigit():
            int_list.append(_sequence_to_int[int(p), int(n)])
    return int_list


def next_rotate_kind(prev: int | None) -> Iterator[int]:
    """整数値で与えられた回転種類から次に取りうる種類を返す."""
    return iter(_next_kind_dict[prev])


_next_kind_dict = {
        None: (0, 1, 2, 3, 4),
        0: (0, 1, 2, 3, 4),
        1: (0, 1, 2),
        2: (0, 1, 2),
        3: (0, 1, 2),
        4: (0, 1, 2),
        }


def rotate_kind_iterator(length: int, prev: int | None = None
                         ) -> Iterator[Sequence[int]]:
    kinds = _next_kind_dict[prev]
    for k in kinds:
        if length > 1:
            for behinds in rotate_kind_iterator(length - 1, k):
                yield (k, *behinds)
        else:
            yield (k, )


def random_rotate_kind_iterator(length: int, prev: int | None = None
                                ) -> Sequence[int]:
    kinds = _next_kind_dict[prev]
    k = kinds[random.randrange(len(kinds))]
    if length > 1:
        return (k, *random_rotate_kind_iterator(length - 1, k))
    else:
        return (k, )
