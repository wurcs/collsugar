"""機械学習のデータ管理"""
from collections.abc import Iterator, Sequence
from typing import IO
from . import iupac
from . import rotama


def read_teacher_data(src: IO[str]) -> Iterator[tuple[str, str, str]]:
    """教師データを文字列形式で読み込む
    三列csv (iupac配列, 回転情報, col | safe)
    """
    for line in src:
        sp = line.split(',')
        if len(sp) == 3:
            yield (sp[0], sp[1], sp[2].strip())


def tearcher_data_str_to_val(iupac_str: str, rotate: str, col: str
                             ) -> tuple[Sequence[int], int]:
    """教師データ1つの文字列形式を数値形式に変換する.

    Returns:
        (data, target)
    """
    is_col = 1 if col == 'col' else 0
    return (data_str_to_val(iupac_str, rotate), is_col)


def data_str_to_val(iupac_str: str, rotate: str,
                    ) -> Sequence[int]:
    """データ1つの文字列形式を数値形式に変換する.

    Returns:
        data
    """
    sugar_chain = iupac.str_sequence_to_int_array(iupac_str)
    rotate_info = rotama.rotate_str_to_int(rotate, len(sugar_chain))
    return (*sugar_chain, *rotate_info)
