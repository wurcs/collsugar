from collections.abc import Sequence


def rotate_str_to_int(rotate_str: str, out_len: int = -1) -> Sequence[int]:
    """回転情報の文字列を整数配列に変換する"""
    # 1-gPhi_3-gPhi_5-gPhi
    sp = rotate_str.split('_')
    max_idx = out_len
    singles: dict[int, int] = dict()
    for single in sp:
        idx, rotate_int = single_rotate_str_to_int(single)
        singles[idx] = rotate_int
        if idx > max_idx:
            max_idx = idx
    rotate_int_list = []
    for i in range(max_idx):
        rotate_int_list.append(singles.get(i, 0))
    return rotate_int_list


def single_rotate_str_to_int(rotate_str: str) -> tuple[int, int]:
    """回転情報文字列の1区切りを整数に変換する

    Returns:
        (回転位置, 回転種類に対応する整数)
    """
    for i in range(len(rotate_str)):
        if not rotate_str[i].isdigit():
            break
    pos = int(rotate_str[:i])
    for j in range(i, len(rotate_str)):
        if not (rotate_str[j] in ('g', 't', '-')):
            break
    kind = _rotate_kind_to_int.get(rotate_str[i:j], None)
    return (pos, kind)


_rotate_kind_to_int: dict[str, int] = {
        'gg': 1,
        'gt': 2,
        'tg': 3,
        't': 4,
        'g': 5,
        '-g': 6,
        }
