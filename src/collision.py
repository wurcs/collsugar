"""3次元のスイープアンドプルーンによる衝突判定"""
from collections.abc import Callable, Iterator
import operator
from .typehint import Sphere


def strict_collision(broad_col: Iterator[tuple[int, int]],
                     id_to_sphere: Callable[[int], Sphere],
                     ) -> Iterator[tuple[int, int]]:
    """球同士の厳密な衝突判定を候補ペアから行う.

    Args:
        broad_col: 衝突候補ペアを表す球のID対
        id_to_sphere: IDから球情報への変換関数
    Return:
        厳密に衝突している球のID対を返すイテレータ
    """
    for o0, o1 in broad_col:
        p0, r0 = id_to_sphere(o0)
        p1, r1 = id_to_sphere(o1)
        if (((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2 + (p0[2] - p1[2])**2)
                < (r0 + r1)**2):
            yield (o0, o1)


def sweep_and_prune(spheres: Iterator[tuple[int, Sphere]],
                    ) -> set[tuple[int, int]]:
    """球同士のおおまかな衝突判定をスイープアンドプルーンで行う.

    Args:
        spheres: 球の集合 (ID, 球のサイズ)
    Returns:
        衝突しているオブジェクトのID対の集合,
        ID対は必ずIDが小さいほうが前にくる.
    """
    ax = _create_sweep_and_prune_axis(spheres)
    col_obj = set(_sweep_and_prune_1d(map(lambda el: el[1:], iter(ax[0]))))
    for dim in range(1, 3):
        col_obj &= set(_sweep_and_prune_1d(
            map(lambda el: el[1:], iter(ax[dim]))))
    return col_obj


def _sweep_and_prune_1d(ax: Iterator[tuple[int, bool]]
                        ) -> Iterator[tuple[int, int]]:
    """1次元の整列済み軸情報を使用して
    1次元のスイープアンドプルーンによるおおまかな衝突判定を行う.

    Args:
        ax: 整列済み軸情報 (ObjectID, 開始点はTrue)
    Returns:
        衝突しているオブジェクトのID対の集合,
        ID対は必ずIDが小さいほうが前にくる.
    """
    cur_opened: set[int] = set()
    for obj_id, is_open in ax:
        if is_open:
            for opened_obj_id in cur_opened:
                yield ((obj_id, opened_obj_id) if obj_id < opened_obj_id
                       else (opened_obj_id, obj_id))
            cur_opened.add(obj_id)
        else:
            cur_opened.discard(obj_id)


def _create_sweep_and_prune_axis(
        spheres: Iterator[tuple[int, Sphere]],
        ) -> tuple[list[tuple[float, int, bool]],
                   list[tuple[float, int, bool]],
                   list[tuple[float, int, bool]]]:
    """各軸の整列済み軸情報を作成する.

    Args:
        spheres: 球の集合 (ID, 球のサイズ)
    Returns:
        (座標, ObjectID, 開始点はTrue)の情報を各軸の座標でソートしたもの
    """
    ax: tuple[list[tuple[float, int, bool]],
              list[tuple[float, int, bool]],
              list[tuple[float, int, bool]]] = ([], [], [])
    for obj_id, (pos, r) in spheres:
        for dim in range(3):
            ax[dim].append((pos[dim] - r, obj_id, True))
            ax[dim].append((pos[dim] + r, obj_id, False))
    for dim in range(3):
        ax[dim].sort(key=operator.itemgetter(0))
    return ax


def liner_collision(obj_ids: Iterator[int],
                    id_to_sphere: Callable[[int], Sphere],
                    ) -> Iterator[tuple[int, int]]:
    """球同士の厳密な衝突判定を線形探索で行う.

    Args:
        obj_ids: 球のID集合
        id_to_sphere: IDから球情報への変換関数
    Returns:
        厳密に衝突している球のID対を返すイテレータ
    """
    obj_ids_array = tuple(obj_ids)
    for i in range(0, len(obj_ids_array) - 1):
        pi, ri = id_to_sphere(i)
        for j in range(i + 1, len(obj_ids_array)):
            pj, rj = id_to_sphere(j)
            if (((pi[0] - pj[0])**2 + (pi[1] - pj[1])**2 + (pi[2] - pj[2])**2)
                    < (ri + rj)**2):
                yield (i, j)
