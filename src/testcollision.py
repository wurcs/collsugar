"""衝突判定のユニットテスト関係"""
import random
import unittest
from . import collision


class TestCollision(unittest.TestCase):

    def test_ab(self):
        size = 1024
        spheres = tuple((tuple(random.uniform(-size, size) for _ in range(3)),
                         random.uniform(size / 64, size / 4))
                        for _ in range(64))
        broad_col = iter(collision.sweep_and_prune(enumerate(iter(spheres))))
        strict_col = collision.strict_collision(
                broad_col, (lambda i: spheres[i]))
        strict_col = set(strict_col)
        liner_col = collision.liner_collision(
                range(len(spheres)), (lambda i: spheres[i]))
        self.assertTrue(
                len(strict_col.symmetric_difference(liner_col)) == 0)
