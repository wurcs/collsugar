"""glycamの生成rotamerを利用した処理"""
from collections.abc import Iterator
import os
import pathlib
import subprocess
import shutil
from typing import IO
from . import chem
from . import collision


def search_glycam_rotamer_out(glycam_dir: os.PathLike
                              ) -> Iterator[tuple[str, os.PathLike]]:
    """glycamで生成したrotamerのディレクトリから出力ファイルを探す.
    .glycam_dir/(回転情報)/*.pdb の形式を想定する.

    Args:
        glycam_dir: glycamの回転情報出力ディレクトリ
    Returns:
        (回転情報, パス)
    """
    for file in glycam_dir.iterdir():
        if file.is_dir():
            rotate_str = file.name
            for p in file.iterdir():
                if p.is_file():
                    yield (rotate_str, p)


def collides_mol(mol: chem.Mol) -> bool:
    """分子オブジェクトの原子に衝突があるか判定する

    Args:
        mol: 分子オブジェクト
    Returns:
        衝突が存在する場合はTrue, それ以外はFalse
    """
    spheres = ((i, mol.get_vdw_sphere_from_atom_idx(i))
               for i in mol.atom_idxs())
    spheres = map(lambda s: (s[0], (s[1][0], 0.40 * s[1][1])), spheres)
    broad_col = collision.sweep_and_prune(spheres)
    col = collision.strict_collision(
        iter(broad_col), mol.get_vdw_sphere_from_atom_idx)
    bonds = set(mol.get_bonds())
    col = filter((lambda c: not (c in bonds)), col)
    try:
        next(col)
        return True
    except StopIteration:
        return False


def write_col_model(data_str: str, out: IO[str], work_dir: pathlib.Path,
                    gen_rotamer_path: pathlib.Path,
                    ) -> None:
    """1つの糖鎖文字列からGlycamを使用してrotamerを生成して衝突判定を行う

    Args:
        data_str: iupac形式の糖鎖文字列
        out: 衝突判定結果の出力先ストリーム (iupac文字列, 回転情報, col | safe)
        work_dir: 一時ファイルの出力ディレクトリ
        gen_rotamer_path: glycamを使用してrotamerを生成するプログラムのパス
    """
    if work_dir.is_dir():
        shutil.rmtree(work_dir)
    # os.makedirs(work_dir, exist_ok=True)
    subprocess.run((os.fsdecode(gen_rotamer_path),
                    '-s', data_str,
                    '-o', os.fsdecode(work_dir)))

    for rotate_str, rotamer_file in search_glycam_rotamer_out(work_dir):
        mol = chem.create_mol_from_file(rotamer_file)
        if mol is None:
            continue
        collides = collides_mol(mol)
        out.write('{},{},{}\n'.format(
            data_str, rotate_str, ('col' if collides else 'safe')))


def gen_rotate_info(iupac_str: str, gen_rotamer_path: pathlib.Path
                    ) -> Iterator[str]:
    """Glycamを使用して回転情報の文字列を生成する.

    Args:
        iupac_str: iupac形式の糖鎖文字列
    Returns:
        回転情報を表す文字列を列挙する
    """
    out = subprocess.run((os.fsdecode(gen_rotamer_path),
                          '-s', iupac_str),
                         capture_output=True, text=True).stdout
    for line in out.split('\n'):
        line = line.strip()
        if len(line) == 0:
            continue
        sp = line.split(',')
        if len(sp) >= 2:
            yield sp[1].strip()
