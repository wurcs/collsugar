"""機械学習の学習部分のメインルーチン"""
import argparse
import pathlib
import pickle
from sklearn import svm
from typing import IO
from . import glycam
from . import iupac
from . import learndata


def create_parser():
    """コマンドラインオプション設定"""
    parser = argparse.ArgumentParser(
            description='教師データの生成または学習モデルの作成を行う')
    parser.add_argument(
            'out_path', help='出力ファイルのパス', type=pathlib.Path)
    parser.add_argument(
            '--genlength', help='生成する教師データの長さ',
            type=int)
    parser.add_argument(
            '--gennum', help='生成する教師データの数(指定しない場合は全探索)',
            type=int)
    parser.add_argument(
            '--teacher', help='入力教師データのパス(モデル精製時)',
            type=pathlib.Path)
    parser.add_argument(
            '--tmpdir', help='作業用tmpディレクトリのパス',
            type=pathlib.Path, default=pathlib.Path('/tmp/collearn'))
    return parser


def generate_learn_model(teacher_data: IO[str],
                         model_out: IO[bytes]) -> None:
    """機械学習を行い学習モデルを保存する.

    Args:
        read_teacher_data: 教師データの入力ストリーム
        model_out: 学習モデルの出力先ストリーム
    """
    data = []
    target = []
    val_itr = map(lambda d: learndata.tearcher_data_str_to_val(*d),
                  learndata.read_teacher_data(teacher_data))
    for d, t in val_itr:
        data.append(d)
        target.append(t)
    model = svm.SVC(gamma="scale")
    model.fit(data, target)
    pickle.dump(model, model_out)


def generate_teacher_data(out: IO[str], work_dir: pathlib.Path,
                          gen_rotamer_path: pathlib.Path,
                          length: int, num: int | None = None) -> None:
    """教師データを生成する.

    Args:
        out: 生成した教師データの出力先
        gen_rotamer_path: glycamを使用してrotamerを生成するプログラムのパス
        length: 糖鎖の単糖数
        num: 生成する糖鎖数, Noneの場合は指定単糖数のすべてを使用する.
    """
    if num is not None:
        for _ in range(num):
            data_str = iupac.generate_sequence(
                    iupac.random_rotate_kind_iterator(length))
            glycam.write_col_model(data_str, out, work_dir, gen_rotamer_path)
    else:
        for k in iupac.rotate_kind_iterator(length):
            data_str = iupac.generate_sequence(k)
            glycam.write_col_model(data_str, out, work_dir, gen_rotamer_path)


def main():
    gen_rotamer_path = (pathlib.Path(__file__).parent.parent
                        / 'glycam_rotamer/genrotamer')
    args = create_parser().parse_args()
    if args.genlength is not None:
        with open(args.out_path, 'w') as teacher:
            generate_teacher_data(teacher, args.tmpdir, gen_rotamer_path,
                                  args.genlength, args.gennum)
    if args.teacher is not None:
        with open(args.out_path, 'wb') as model_out, \
                open(args.teacher, 'r') as teacher:
            generate_learn_model(teacher, model_out)
