% 糖鎖のrotamer衝突判定プログラム
% 2023/03

# 概要
糖鎖のrotamerはグリコシド結合の数が増えるに従って候補が加速度的に増えていく。
個別のrotamer立体配置に対して衝突判定を行うと計算時間が増大する可能性がある。
これを解消するため、本プログラムは機械学習により糖鎖のiupac文字列のみで簡易的に衝突の判別を行うことを目的としている。

# アルゴリズム
機械学習によってiupac文字列から衝突判定を行わずに衝突の予想を行う。  

教師データとしては糖鎖に共通の根元部分の衝突を扱うため、異なる立体構造を持つ以下の5種の結合から可能な組み合わせを用いる。

~~~~~~~~~~~~~~~~
DGalpb1-4
DGalpa1-3
DGalpNAca1-6
DNeup5Aca2-3
DNeup5Aca2-6
~~~~~~~~~~~~~~~~

教師データの糖鎖からGlycamを使用してrotamerを生成して衝突判定を行う。
教師データの衝突判定は,共有結合を形成していない原子間の距離が共有結合を形成している場合より近い場合衝突しているとみなす。この条件は2原子間の距離がファンデルワールス半径の合計の0.4倍より近い場合で近似している。

機械学習はサポートベクターマシンを使用して、結合と回転の情報ベクトルを衝突しているかどうかで分類する問題として計算している。  
入力糖鎖が学習に使用した糖鎖より長い場合、学習した最大長の部分構造に対して機械学習を適応する。部分構造を1単糖毎にスライドさせ、すべての衝突予測で衝突が無いと判定した場合のみ全体でも衝突が無いと判断する。

# ファイル構成
* collsugar/
    * collsugar.py : 衝突判定の起動スクリプト
    * learn.py : 学習モデル作成スクリプト
    * src/ : ソースコード本体
    * pyproject.toml : poetry用依存pythonパッケージ情報
    * glycam_rotamer : Glycamを使用したromater生成プログラム

# インストール方法
## glycam_rotamer
collsugar/glycam_rotamer/Makefile を編集する。  
GEMSHOME = にGlycamのインストール先ディレクトリを指定する。  

collsugar/glycam_rotamer/ で make コマンドを実行すると  
collsugar/glycam_rotamer/genrotamer が生成される

## collsugar
poetry を利用する。  
collsugar/ ディレクトリで以下のコマンドを実行する。

~~~~~~~~~~~~~~~~
poetry install
~~~~~~~~~~~~~~~~

# 使用方法
## 教師データの作成
collsugar/learn.py を利用する。

~~~~~~~~~~~~~~~~
usage: learn.py [-h] [--genlength GENLENGTH] [--gennum GENNUM]
                [--teacher TEACHER] [--tmpdir TMPDIR]
                out_path

positional arguments:
  out_path              出力ファイルのパス

options:
  -h, --help            show this help message and exit
  --genlength GENLENGTH
                        生成する教師データの長さ
  --gennum GENNUM       生成する教師データの数(指定しない場合は全探索)
  --teacher TEACHER     入力教師データのパス(モデル精製時)
  --tmpdir TMPDIR       作業用tmpディレクトリのパス
~~~~~~~~~~~~~~~~

collsugar/ディレクトリで以下のコマンドを実行する。

~~~~~~~~~~~~~~~~
mkdir teacher
poetry run python learn.py --genlength 2 teacher/2.txt
poetry run python learn.py --genlength 3 teacher/3.txt
...
poetry run python learn.py --genlength 6 teacher/6.txt
poetry run python learn.py --genlength 7 --gennum 800 teacher/7.txt
poetry run python learn.py --genlength 8 --gennum 600 teacher/8.txt
poetry run python learn.py --genlength 9 --gennum 400 teacher/9.txt
~~~~~~~~~~~~~~~~

collsugar/teacher/ ディレクトリ直下に教師データが生成される。

## 学習モデルの作成
collsugar/learn.py を利用する。  
collsugarディレクトリで以下のコマンドを実行する。

~~~~~~~~~~~~~~~~
mkdir model
poetry run python learn.py --teacher teacher/2.txt model/2.pkl
poetry run python learn.py --teacher teacher/3.txt model/3.pkl
...
poetry run python learn.py --teacher teacher/9.txt model/9.pkl
~~~~~~~~~~~~~~~~

collsugar/model/ ディレクトリ直下に学習モデルが作成される。

## 学習モデルによる判別
collsugar/collsugar.py を利用する。  

~~~~~~~~~~~~~~~~
usage: collsugar.py [-h] [--input INPUT] [--tmpdir TMPDIR] out_path model

positional arguments:
  out_path         出力ファイルのパス
  model            学習済みモデルの保存ディレクトリ

options:
  -h, --help       show this help message and exit
  --input INPUT    入力ファイルのパス
  --tmpdir TMPDIR  作業用tmpディレクトリのパス
~~~~~~~~~~~~~~~~

iupac形式の糖鎖文字列を1行づつ記録したファイルのパスを  
/home/glyconavi/input_IUPAC/N-Glycan.GlycanWeb.iupac とする。  
collsugarディレクトリで以下のコマンドを実行する。

~~~~~~~~~~~~~~~~
poetry run python collsugar.py out.csv ./model --input /home/glyconavi/input_IUPAC/N-Glycan.GlycanWeb.iupac
~~~~~~~~~~~~~~~~

衝突判定結果がcollsugar/out.csvに出力される。  
判定結果は以下の形式になる。  
(iupac糖鎖文字列),(rotamer回転情報),(衝突している場合はcol, それ以外はsafe)

例
~~~~~~~~~~~~~~~~
DManpa1-3[DManpa1-6]DManpb1-4DGlcpNAcb1-4DGlcpNAcb1-OH,4ggOmg,col
DManpa1-3[DManpa1-6]DManpb1-4DGlcpNAcb1-4DGlcpNAcb1-OH,4gtOmg,col
~~~~~~~~~~~~~~~~
