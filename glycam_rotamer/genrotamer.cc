#include "includes/gmml.hpp"
#include "cxxopts.hpp"
#include <iostream>
#include <string>
#include <filesystem>
#include <optional>

void GenerateSpecific3DStructure(
        CondensedSequence::carbohydrateBuilder &builder,
        CondensedSequence::SingleRotamerInfoVector conformerInfo,
        std::string fileOutputDirectory);

/**
 * CondensedSequence::carbohydrateBuilder::GenerateUserOptionsDataStruct()
 * の出力から取りうる回転に対応するSingleRotamerInfoVectorを列挙する.
 * C++のイテレータは自作に手間がかかるためnext()関数のみの簡易構成で
 * イテレータ風実装をしている.
 *
 * 回転場所は(DihedralOptions), 回転角は(DihedralOptions::rotamers_)
 * 回転場所数分の階層のループが必要になるが、変数で階層が決まるループの実装は
 * ループ階層のスタックをヒープメモリ上に自前実装することです.
 * これには再帰呼び出し(相当のもの)を使います.
 * ここではイテレータ化したクラスでループ部分を書いて計算ロジックと
 * 分離しています.
 */
class SingleRotamerInfoVectorGenerator {
private:
    struct MyBuf {
        const CondensedSequence::LinkageOptions& linkage;
        const CondensedSequence::DihedralOptions& dihedral;
        std::vector<std::string>::const_iterator rotamer_itr;
    };

    CondensedSequence::LinkageOptionsVector src_;
    std::vector<MyBuf> buf_;

public:
    SingleRotamerInfoVectorGenerator(
            const CondensedSequence::LinkageOptionsVector& src) {
        src_ = CondensedSequence::LinkageOptionsVector(src);
        for (auto &linkage : src_) {
            for (auto &dihedral : linkage.likelyRotamers_) {
                if ((*dihedral.rotamers_.begin()).size() == 1) {
                    char ch = (*dihedral.rotamers_.begin())[0];
                    if (isupper(ch)) {
                        continue;
                    }
                }
                buf_.push_back(MyBuf{
                        linkage,
                        dihedral,
                        dihedral.rotamers_.begin()
                });
            }
        }
    }

    std::optional<std::vector<CondensedSequence::SingleRotamerInfo>> next() {
        if ((buf_.size() == 0) ||
                (buf_[0].rotamer_itr == buf_[0].dihedral.rotamers_.end())) {
            return std::nullopt;
        }
        auto ret = buf_vec_to_data();
        increment_iterator(buf_.size() - 1);
        return ret;
    }

private:
    std::vector<CondensedSequence::SingleRotamerInfo>
            buf_vec_to_data() const {
        std::vector<CondensedSequence::SingleRotamerInfo> ret;
        for (auto& b : buf_) {
            ret.push_back(CondensedSequence::SingleRotamerInfo {
                    b.linkage.indexOrderedLabel_,
                    b.linkage.linkageName_,
                    b.dihedral.dihedralName_,
                    *b.rotamer_itr,
                    ""
            });
        }
        return ret;
    }

    void increment_iterator(int idx) {
        auto& b = buf_[idx];
        ++b.rotamer_itr;
        if (b.rotamer_itr == b.dihedral.rotamers_.end()) {
            if (idx == 0) {
                return;
            } else {
                return increment_iterator(idx - 1);
            }
        } else {
            for (unsigned int i = idx + 1; i < buf_.size(); ++i) {
                buf_[i].rotamer_itr = buf_[i].dihedral.rotamers_.begin();
            }
        }
    }
};


/**
 * 回転情報に1対1対応する適当な名前を生成する.
 */
std::string single_rotamer_info_vector_to_dirname(
        const CondensedSequence::SingleRotamerInfoVector& v) {
    std::string buf;
    for (unsigned int i = 0; i < v.size(); ++i) {
        if (i > 0) {
            buf += '_';
        }
        buf += v[i].linkageIndex;
        buf += v[i].selectedRotamer;
        buf += v[i].dihedralName;
    }
    return buf;
}


int gen_rotamer(const std::string &condensed_sequence,
                const std::string &prep,
                const std::optional<std::filesystem::path> &out_dir,
                const std::optional<std::string> &rotate_only_name,
                bool verbose
                ) {
    auto& cout = std::cout;
    auto& cerr = std::cerr;

    CondensedSequence::carbohydrateBuilder carbBuilder(
            condensed_sequence, prep);
    carbBuilder.Print();
    if (out_dir && !std::filesystem::create_directory(out_dir.value())) {
        cerr << "error mkdir : " << out_dir.value() << '\n';
    }
    auto userOptions = carbBuilder.GenerateUserOptionsDataStruct();
    if (verbose) {
        cout << "Number of likely shapes for this sequence is "
             << carbBuilder.GetNumberOfShapes(true) << '\n';
        // Default is to calculate all possible.
        cout << "Number of possible shapes for this sequence is "
             << carbBuilder.GetNumberOfShapes() << '\n';

        for (auto &linkageInfo : userOptions) {
            cout << "Name: " << linkageInfo.linkageName_
                 << ", LinkageIndex: " << linkageInfo.indexOrderedLabel_
                 << ", Res1: " << linkageInfo.firstResidueNumber_
                 << ", Res2: " << linkageInfo.secondResidueNumber_ << '\n';
            for(auto &dihedralInfo : linkageInfo.likelyRotamers_) {
                cout << "    LikelyRotamers: " << dihedralInfo.dihedralName_;
                for (auto &rotamer : dihedralInfo.rotamers_) {
                    cout << ", " << rotamer;
                }
                cout << '\n';
            }
        }
    }
    // 出力用の計算はここから.
    auto gen_info = SingleRotamerInfoVectorGenerator(userOptions);
    while (true) {
        auto ret = gen_info.next();
        if (!ret) {
            break;
        }
        auto rotamer_vec = ret.value();
        auto rotamer_name = single_rotamer_info_vector_to_dirname(rotamer_vec);
        if (rotate_only_name && (rotamer_name != rotate_only_name.value())) {
            continue;
        }
        if (out_dir) {
            auto rotamer_out_dir = out_dir.value() / rotamer_name;
            if (!std::filesystem::create_directory(rotamer_out_dir)) {
                cerr << "error mkdir : " << rotamer_out_dir << '\n';
            }
            GenerateSpecific3DStructure(
                    carbBuilder, rotamer_vec, rotamer_out_dir.string());
        } else {
            cout << condensed_sequence << ','
                 << rotamer_name << '\n';
        }
    }
    if (verbose) {
        cout << "Fin\n";
    }
    return 0;
}


int main(int argc, char *argv[]) {
    cxxopts::Options options("genrotamer");
    options.add_options()
        ("o,outdir", "output directory",  cxxopts::value<std::string>())
        ("s,sequence", "sequence string", cxxopts::value<std::string>())
        ("r,rotateonly", "output only pdb that match rotate string",
         cxxopts::value<std::string>())
        ("v,verbose", "output verbose info to stdout")
        ("h,help", "print usage");
    auto result = options.parse(argc, argv);
    if (result.count("help")) {
        std::cout << options.help() << std::endl;
        exit(0);
    }

    try {
        std::optional<std::filesystem::path> out_dir;
        try {
            out_dir = std::filesystem::path(
                    result["outdir"].as<std::string>());
        } catch (cxxopts::exceptions::exception &e) {
            out_dir = std::nullopt;
        }
        std::optional<std::string> rotate_only_name;
        try {
            rotate_only_name = result["rotateonly"].as<std::string>();
        } catch (cxxopts::exceptions::exception &e) {
            rotate_only_name = std::nullopt;
        }
        return gen_rotamer(
                result["sequence"].as<std::string>(),
                std::string(GEMESHOME "/dat/prep/GLYCAM_06j-1.prep"),
                out_dir,
                rotate_only_name,
                result["verbose"].as<bool>());
    } catch(...) {
        std::cerr << "genrotamer error\n";
        return 1;
    }
}


Residue_linkage* selectLinkageWithIndex(
        ResidueLinkageVector &inputLinkages, int indexQuery)
{
    for(auto &linkage : inputLinkages)
    {
        if (linkage.GetIndex() == indexQuery)
            return &linkage;
    }
    // Error
    std::stringstream ss;
    ss << "Linkage numbered " << indexQuery << " not found in linkages for this carbohydrate\n";
    gmml::log(__LINE__,__FILE__,gmml::ERR, ss.str());
    throw std::runtime_error(ss.str());
}


std::string convertIncomingRotamerNamesToStandard(std::string incomingName)
{   // Lamda function to see if string is in the passed in vector
    auto isAinList = [](std::vector<std::string> names, std::string query)
    {
        if (std::find(names.begin(), names.end(), query) != names.end())
            return true;
        return false;
    }; // Thought about converting incomingName to lowercase first.
    if (isAinList({"Omega", "omega", "Omg", "omg", "OMG", "omga", "omg1", "Omg1", "o"}, incomingName))
        return "Omg";
    if (isAinList({"Phi", "phi", "PHI", "h"}, incomingName))
        return "Phi";
    if (isAinList({"Psi", "psi", "PSI", "s"}, incomingName))
        return "Psi";
    if (isAinList({"Chi1", "chi1", "CHI1", "c1"}, incomingName))
        return "Chi1";
    if (isAinList({"Chi2", "chi2", "CHI2", "c2"}, incomingName))
        return "Chi2";
    if (isAinList({"Omega7", "omega7", "OMG7", "omga7", "Omg7", "omg7", "o7"}, incomingName))
        return "Omg7";
    if (isAinList({"Omega8", "omega8", "OMG8", "omga8", "Omg8", "omg8", "o8"}, incomingName))
        return "Omg8";
    if (isAinList({"Omega9", "omega9", "OMG9", "omga9", "Omg9", "omg9", "o9"}, incomingName))
        return "Omg9";
    std::stringstream ss;
    ss << "Specified rotamer name: \"" << incomingName << "\", is not recognized in convertIncomingRotamerNamesToStandard.";
    throw ss.str();
}


void Write3DStructureFile(
        CondensedSequence::carbohydrateBuilder &builder,
        std::string fileOutputDirectory,
        std::string fileType,
        std::string filename)
{
    // Build the filename and path, add appropriate suffix later
    std::string completeFileName;
    if (fileOutputDirectory != "unspecified") // "unspecified" is the default
    {
        completeFileName += fileOutputDirectory + "/";
    }
    completeFileName += filename;
    //std::cout << "Will write out " << completeFileName << ". Which is of type: " << fileType << "\n";
    // Use type to figure out which type to write, eg. PDB OFFFILE etc.
    if (fileType == "PDB") 
    { // int link_card_direction = -1, int connect_card_existance = 1, int model_index = -1 , bool useInputPDBResidueNumbers = true);
        PdbFileSpace::PdbFile *outputPdbFile = builder.GetAssembly()->BuildPdbFileStructureFromAssembly(-1, 1,-1, false);
        completeFileName += ".pdb";
        outputPdbFile->Write(completeFileName);
    }
    return;
}


void GenerateSpecific3DStructure(
        CondensedSequence::carbohydrateBuilder &builder,
        CondensedSequence::SingleRotamerInfoVector conformerInfo,
        std::string fileOutputDirectory)
{
    //     std::string linkageIndex; // What Dan is calling linkageLabel. Internal index determined at C++ level and given to frontend to track.
    //     std::string linkageName; // Can be whatever the user wants it to be, default to same as index.
    //     std::string dihedralName; // omg / phi / psi / chi1 / chi2
    //     std::string selectedRotamer; // gg / tg / g- etc
    //     std::string numericValue; // user entered 64 degrees. Could be a v2 feature.
    // With a conformer (aka rotamerSet), setting will be different as each rotatable_dihedral will be set to e.g. "A", whereas for linkages
    // with combinatorial rotamers (e,g, phi -g/t, omg gt/gg/tg), we need to set each dihedral as specified, but maybe it will be ok to go 
    // through and find the value for "A" in each rotatable dihedral.. yeah actually it should be fine. Leaving comment for time being.
    try
    {
        for (auto &rotamerInfo : conformerInfo)
        {
            int currentLinkageIndex = std::stoi(rotamerInfo.linkageIndex);
            Residue_linkage *currentLinkage = selectLinkageWithIndex(
                    *(builder.GetGlycosidicLinkages()), currentLinkageIndex);
            std::string standardDihedralName = convertIncomingRotamerNamesToStandard(rotamerInfo.dihedralName);
            currentLinkage->SetSpecificShape(standardDihedralName, rotamerInfo.selectedRotamer);
        }
        std::string fileName = "structure";
        //this->ResolveOverlaps();
        Write3DStructureFile(builder, fileOutputDirectory, "PDB", fileName);
        //this->Write3DStructureFile(fileOutputDirectory, "OFFFILE", fileName);
    }   // Better to throw once I figure out how to catch it in gems. This setting status thing and checking it is a bad pattern.
    catch (...)
    {}
    return;
}
